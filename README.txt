CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainersss


INTRODUCTION
------------

Continuous Rss Scrolling drupal module will create the 
vertical scrolling reel text show in the drupal website 
using the given rss feed. This will scroll the rss feed 
title like reel. The title will scroll one by one in the 
selected block position. Using this module we can show 
any rss feed in our website. No coding knowledge required 
to configure this module.

There is text box option available to enter the RSS feed 
into the module. Please follow the module installation 
steps for further instruction or visit the module 
installation video using below available link.


FEATURES:
---------

1. Easy installation and customization
2. Have option to add any rss feed.
3. Easy scroller setting for smooth scroller

REQUIREMENTS
------------

Just Drupal module Continuous Rss Scrolling moudule files


INSTALLATION
------------

1. Install the module as normal, see link for instructions.
Link: https://www.drupal.org/documentation/install/
modules-themes/modules-8


CONFIGURATION
-------------

1. Go to Admin >> Extend page and check the Enabled check 
box near to the module Continuous Rss Scrolling and then 
click the Install button at the bottom.

2. Go to Admin >> Structure >> Block layout page, there 
you can see button called Place Block near each available 
blocks.

3. Go to Admin >> Structure >> Blocks layout page and click 
Place Block button to add Block. If you want to add Block in 
your first sidebar, click Place Block button near your Sidebar 
first title. It will open Place block window. In that window 
again click Place Block button near Continuous Rss Scrolling.

4. Now open your website front end and see the Continuous RSS 
scrolling module in the selected location. At first it use 
the default RSS link, go to configuration link of the 
module and update the default module setting. see the below 
screen.


MAINTAINERS
-----------

Current maintainers:

 * Gopi RAMASAMY 
https://www.drupal.org/user/1388160
http://www.gopiplus.com/work/about/
http://www.gopiplus.com/extensions/2012/02/drupal-continuous
-vertical-rss-scrolling-module-for-drupal-cms/
 
Requires - Drupal 8
License - GPL (see LICENSE)
