/**
 * @name : Continuous Rss Scrolling
 * @author : Gopi RAMASAMY
 * @version : 1.0
 * @url : http://www.gopiplus.com/extensions/2012/02/drupal-continuous-vertical-rss-scrolling-module-for-drupal-cms/
 * @license : MIT License
 */

function crs_scroll() {
  'use strict';
  crs_obj.scrollTop = crs_obj.scrollTop + 1;
  crs_scrollPos++;
  if ((crs_scrollPos%crs_heightOfElm) == 0) {
    crs_numScrolls--;
    if (crs_numScrolls == 0) {
      crs_obj.scrollTop = '0';
      crs_content();
    }
    else {
      if (crs_scrollOn == 'true') {
        crs_content();
      }
    }
  }
  else {
    // Speed values: 10 slow, 50 fast
    var speed = 60 - ( crs_speed * 10 );
    setTimeout('crs_scroll();', speed);
  }
}

var crs_Num = 0;
/*
Creates amount to show + 1 for the scrolling ability to work
scrollTop is set to top position after each creation
Otherwise the scrolling cannot happen
*/
function crs_content() {
  'use strict';
  var tmp_vsrp = '';

  w_vsrp = crs_Num - parseInt(crs_numberOfElm);
  if (w_vsrp < 0) {
    w_vsrp = 0;
  }
  else {
    w_vsrp = w_vsrp%crs_array.length;
  }

  // Show amount of vsrru
  var elementsTmp_vsrp = parseInt(crs_numberOfElm) + 1;
  for (i_vsrp = 0; i_vsrp < elementsTmp_vsrp; i_vsrp++) {
    tmp_vsrp += crs_array[w_vsrp%crs_array.length];
    w_vsrp++;
  }

  crs_obj.innerHTML = tmp_vsrp;
  crs_Num = w_vsrp;
  crs_numScrolls = crs_array.length;
  crs_obj.scrollTop = '0';
  // start scrolling
  setTimeout('crs_scroll();', crs_waitseconds * 2000);
}
