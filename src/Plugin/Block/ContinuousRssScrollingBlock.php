<?php

namespace Drupal\continuous_rss_scrolling\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'ContinuousRssScrolling' Block.
 *
 * @Block(
 * id = "continuous_rss_scrolling",
 * admin_label = @Translation("Continuous Rss Scrolling"),
 * )
 */
class ContinuousRssScrollingBlock extends BlockBase implements BlockPluginInterface {

  /**
  * {@inheritdoc}
  */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['crs_rsslink'])) {
      $crs_rsslink = $config['crs_rsslink'];
    }
    else {
      $crs_rsslink = $this->t('No valid rss feed');
    }

    if (!empty($config['crs_record_height'])) {
      $crs_record_height = $config['crs_record_height'];
    }
    else {
      $crs_record_height = 50;
    }

    if (!empty($config['crs_display_count'])) {
      $crs_display_count = $config['crs_display_count'];
    }
    else {
      $crs_display_count = 5;
    }

    if (!empty($config['crs_display_width'])) {
      $crs_display_width = $config['crs_display_width'];
    }
    else {
      $crs_display_width = 200;
    }

    if (!empty($config['crs_speed'])) {
      $crs_speed = $config['crs_speed'];
    }
    else {
      $crs_speed = 2;
    }

    if (!empty($config['crs_waitseconds'])) {
      $crs_waitseconds = $config['crs_waitseconds'];
    }
    else {
      $crs_waitseconds = 2;
    }

    $output[]['#cache']['max-age'] = 0;
    $values = [
      'crs_rsslink' => $crs_rsslink,
      'crs_record_height' => $crs_record_height,
      'crs_display_count' => $crs_display_count,
      'crs_display_width' => $crs_display_width,
      'crs_speed' => $crs_speed,
      'crs_waitseconds' => $crs_waitseconds,  
    ];

    $markup = $this->crsBlock($values);
    $output[] = [
      '#markup' => t($markup) , 
      '#allowed_tags' => ['script', 'div', 'a', 'span', 'img', 'p'],
    ];
    $output['#attached']['library'][] = 'continuous_rss_scrolling/continuous_rss_scrolling';
    return $output;
  }
  
  /**
   * {@inheritdoc}
   */
  private function  crsBlock(array $values) {

    $crs_rsslink = $values['crs_rsslink'];
    $crs_record_height = $values['crs_record_height'];
    $crs_display_count = $values['crs_display_count'];
    $crs_display_width = $values['crs_display_width'];
    $crs_speed = $values['crs_speed'];
    $crs_waitseconds = $values['crs_waitseconds'];

    if ($crs_rsslink == "") {
      return false;
    }

    if (!is_numeric($crs_record_height)) {
      $crs_record_height = 50;
    }

    if (!is_numeric($crs_display_count)) {
      $crs_display_count = 5;
    }

    if (!is_numeric($crs_display_width)) {
      $crs_display_width = 200;
    }

    if (!is_numeric($crs_speed)) {
      $crs_speed = 2;
    }

    if (!is_numeric($crs_waitseconds)) {
      $crs_waitseconds = 2;
    }

    $xml = "";
    $cnt = 0;
    $f = fopen( $crs_rsslink, 'r' );
    while($data = fread($f, 4096)) { 
      $xml .= $data; 
    }
    fclose( $f );
    preg_match_all("/\<item\>(.*?)\<\/item\>/s", $xml, $itemblocks );

    if (! empty($itemblocks)) {
      $crs_count = 0;
      $crs_html = "";
      $crs_array = "";

      foreach( $itemblocks[1] as $block ) {
        preg_match_all("/\<title\>(.*?)\<\/title\>/",  $block, $title);
        preg_match_all("/\<link\>(.*?)\<\/link\>/", $block, $link);
        preg_match_all("/\<description\>(.*?)\<\/description\>/", $block, $description);

        $crs_post_title = $title[1][0];
        $get_permalink = $link[1][0];
        //echo $description[1][0];

        $crs_post_title = substr($crs_post_title, 0, $crs_display_width);
        $dis_height = $crs_record_height."px";
        $crs_html = $crs_html . "<div class='crs_div' style='height:$dis_height;padding:2px 0px 2px 0px;'>"; 
        $crs_html = $crs_html . "<a target='_blank' href='$get_permalink'>$crs_post_title</a>";
        $crs_html = $crs_html . "</div>";

        $crs_post_title = trim($crs_post_title);
        $get_permalink = $get_permalink;
        $crs_array = $crs_array . "crs_array[$crs_count] = '<div class=\'crs_div\' style=\'height:$dis_height;padding:2px 0px 2px 0px;\'><a target=\'_blank\' href=\'$get_permalink\'>$crs_post_title</a></div>'; ";	
        $crs_count++;
      }

      $crs_record_height = $crs_record_height + 4;
      if($crs_count >= $crs_display_count) {
        $crs_count = $crs_display_count;
        $crs_height = ($crs_record_height * $crs_display_count);
      }
      else {
        $crs_count = $crs_count;
        $crs_height = ($crs_count * $crs_record_height);
      }
      $crs_height1 = $crs_record_height."px";
  
      $ctsop = "";
      //$ctsop = $ctsop . '<div>';
      $ctsop = $ctsop . '<div  id="crs_Holder" style="text-align:left;vertical-align:middle;text-decoration: none;overflow: hidden; position: relative; margin-left: 1px; height: '.$crs_height1.';">';
      $ctsop = $ctsop . $crs_html;
      $ctsop = $ctsop . '</div>';
      //$ctsop = $ctsop . '</div>';
  
      $ctsop = $ctsop . '<script type="text/javascript">';
      $ctsop = $ctsop . 'var crs_array	= new Array();';
      $ctsop = $ctsop . "var crs_obj	= '';";
      $ctsop = $ctsop . "var crs_scrollPos 	= '';";
      $ctsop = $ctsop . "var crs_numScrolls	= '';";
      $ctsop = $ctsop . "var crs_heightOfElm = '" . $crs_record_height."';";
      $ctsop = $ctsop . "var crs_numberOfElm = '" . $crs_count."';";
      $ctsop = $ctsop . "var crs_speed 		= '" . $crs_speed . "';";
      $ctsop = $ctsop . "var crs_waitseconds = '" . $crs_waitseconds . "';";
      $ctsop = $ctsop . "var crs_scrollOn 	= 'true';";
      $ctsop = $ctsop . 'function crs_createscroll()';
      $ctsop = $ctsop . '{';
      $ctsop = $ctsop . $crs_array;
      $ctsop = $ctsop . "crs_obj	= document.getElementById('crs_Holder');";
      $ctsop = $ctsop . "crs_obj.style.height = (crs_numberOfElm * crs_heightOfElm) + 'px';";
      $ctsop = $ctsop . 'crs_content();';
      $ctsop = $ctsop . '}';
      $ctsop = $ctsop . '</script>';
      $ctsop = $ctsop . '<script type="text/javascript">';
      $ctsop = $ctsop . 'crs_createscroll();';
      $ctsop = $ctsop . '</script>';
    }
    else {
      $ctsop = 'No valid rss feed';
    }
    return $ctsop;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['crs_rsslink'] = [
      '#type' => 'textfield',
      '#title' => $this->t('RSS feed link'),
      '#description' => $this->t('Enter the RSS link to scroll the title.'),
      '#default_value' => isset($config['crs_rsslink']) ? $config['crs_rsslink'] : 'http://www.gopiplus.com/work/category/word-press-plug-in/feed/'
    ];

    $form['crs_record_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scroll height'),
      '#description' => $this->t('If any overlap in the scroll, you should arrange(increase/decrease) this height (example: 50).'),
      '#default_value' => isset($config['crs_record_height']) ? $config['crs_record_height'] : '50'
    ];

    $form['crs_display_count'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display count'),
      '#description' => $this->t('Please enter number of records you want to display at the same time in scroll (example: 2).'),
      '#default_value' => isset($config['crs_display_count']) ? $config['crs_display_count'] : '2'
    ];

    $form['crs_display_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display length'),
      '#description' => $this->t('Please enter max number of character to display in the scroll (example: 200).'),
      '#default_value' => isset($config['crs_display_width']) ? $config['crs_display_width'] : '200'
    ];

    $form['crs_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scrolling speed'),
      '#description' => $this->t('Enter how fast you want the to scroll the items (1 to 10).'),
      '#default_value' => isset($config['crs_speed']) ? $config['crs_speed'] : '2'
    ];

    $form['crs_waitseconds'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Seconds to wait'),
      '#description' => $this->t('How many seconds you want to wait the scroll (example: 2).'),
      '#default_value' => isset($config['crs_waitseconds']) ? $config['crs_waitseconds'] : '2'
    ];

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['crs_rsslink'] = $form_state->getValue('crs_rsslink');
    $this->configuration['crs_record_height'] = $form_state->getValue('crs_record_height');
    $this->configuration['crs_display_count'] = $form_state->getValue('crs_display_count');
    $this->configuration['crs_display_width'] = $form_state->getValue('crs_display_width');
    $this->configuration['crs_speed'] = $form_state->getValue('crs_speed');
    $this->configuration['crs_waitseconds'] = $form_state->getValue('crs_waitseconds');
  }
  
}
